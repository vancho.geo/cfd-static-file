# cfd-static-file

Terraform files for Penni.io's interview home assignment. 

# Prerequisites

* Hosted zone in Route53, NS records configured at the the domain registrar. Replace "demolabs.website"
* IAM user with admin permissions, required for profile. Replace "vancho@devopsvan"
* S3 bucket and DynamoDB table for managing TF state. Replace "eu-devopsvan-tfstate" and "your-terraform-lock-table" respectively

