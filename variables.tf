variable "aws_profile" {
    type = string
    default = "vancho@devopsvan"
}

variable "root_zone" {
    type = string
    default = "demolabs.website"
}

variable "bucket_name" {
    type = string
    default = "demo-bucket-asetofrandomcharactersgoeshere"
}
