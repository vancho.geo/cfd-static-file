data "aws_iam_policy_document" "demo_bucket_policy" {
  statement {
    sid       = "DemoBucketPolicy123120931"
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.demo_bucket.arn}/*"]
    #sid 

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.demo_oai.iam_arn]
    }
  }
}

data "aws_route53_zone" "demo_zone" {
    name         = var.root_zone
    private_zone = false
}