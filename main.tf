provider "aws" {
  region  = "eu-central-1"
  profile = "vancho@devopsvan"
}

provider "aws" {
  region  = "us-east-1"
  profile = "vancho@devopsvan"
  alias   = "us-east-1"
}

terraform {
  backend "s3" {
    // bucket and table managed outside of terraform
    bucket         = "eu-devopsvan-tfstate"
    key            = "static-file-hosting.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "your-terraform-lock-table"
    profile        = "vancho@devopsvan"
  }
}

#Create record for CloudFormation and add association for CloudFront
resource "aws_route53_zone" "demo_zone" {
  name = var.root_zone
  lifecycle { 
    prevent_destroy = true
  }

}

#Create a bucket to host the static file
resource "aws_s3_bucket" "demo_bucket" {
  bucket  = var.bucket_name
  acl     = "private"
  tags    = locals.tags

  server_side_encryption_configuration{ 
    rule{
      apply_server_side_encryption_by_default{
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_object_copy" "demo_file" {
  bucket  = var.bucket_name
  key     = "2_MB_file.jpg"
  source  = "demo-bucket-source-12390120912839123123/2_MB_file.jpg"
}


#Block S3 bucket-level Public Access 
resource "aws_s3_bucket_public_access_block" "public_block" {
  bucket                  = aws_s3_bucket.demo_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

#Apply S3 policy to restrict bucket access to CFD only
resource "aws_s3_bucket_policy" "demo_bucket_policy" {
  bucket = aws_s3_bucket.demo_bucket.id
  policy = data.aws_iam_policy_document.demo_bucket_policy.json
}

#Create a cloudfront distribution
resource "aws_cloudfront_distribution" "demo_distribution" {
  origin {
    domain_name = aws_s3_bucket.demo_bucket.bucket_regional_domain_name
    origin_id   = aws_s3_bucket.demo_bucket.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.demo_oai.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Demo CF distribution for penni.io"
  default_root_object = "index.html"

/*  
    Uncomment and configure bucket demo_logs_bucketrandomstring if logging was required
    logging_config {
    include_cookies = false
    bucket          = "demo_logs_bucketrandomstring.s3.amazonaws.com"
    prefix          = "logslivehere"
  }
*/

  
  aliases = ["static.${var.root_zone}"]

  
  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.demo_bucket.bucket_regional_domain_name
 
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
    
    min_ttl                = 0
    default_ttl            = 60 #Reduced from 3600 for testing purposes
    max_ttl                = 86400
    viewer_protocol_policy = "redirect-to-https"
  }
  
  price_class = "PriceClass_100" #Requirement was to enable it only in EU and US regions

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["DK", "US", "CA", "DE", "GB", "SI"]
    }

  tags = locals.tags

  }
  
  viewer_certificate {
    cloudfront_default_certificate  = false
    acm_certificate_arn             = aws_acm_certificate.demo_cert.arn
    ssl_support_method              = "sni-only"
    minimum_protocol_version        = "TLSv1.2_2021"  #"TLSv1.2_2018"
  }
}

resource "aws_cloudfront_origin_access_identity" "demo_oai" {
  comment = "Origin Access Identity for ${var.root_zone}"
}

resource "aws_acm_certificate" "demo_cert" {
  domain_name               = var.root_zone
  subject_alternative_names = ["*.${var.root_zone}"]
  validation_method         = "DNS"
  provider                  = aws.us-east-1 #Can be created only in us-east-1 region
  tags                      = locals.tags
}

#Create a certificate validation record
resource "aws_route53_record" "demo_validator" {
  for_each = {
    for dvo in aws_acm_certificate.demo_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.demo_zone.zone_id
}

resource "aws_acm_certificate_validation" "demo_validator" {
  certificate_arn         = aws_acm_certificate.demo_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.demo_validator : record.fqdn]
  provider                = aws.us-east-1
}

resource "aws_route53_record" "staticsite" {
  name    = "static.${var.root_zone}"
  zone_id = data.aws_route53_zone.demo_zone.zone_id
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.demo_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.demo_distribution.hosted_zone_id
    evaluate_target_health = true
  }
}

locals {
  tags = {
    Environment = "Demo"
  }
}